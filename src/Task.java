import java.util.Arrays;

public class Task {

    public static void main(String[] args) {
        Node[] nodes = {
                new Node(1, "Parent1", -1),
                new Node(11, "Child1-1", 1),
                new Node(12, "Child1-2", 1),
                new Node(13, "SubParent1", 1),
                new Node(131, "Child1-1-1", 13),
                new Node(132, "Child1-1-2", 13),
                new Node(21, "Child2-1", 2),
                new Node(3, "Parent3", -1),
                new Node(2000, "Child2-2", 2),
                new Node(400, "Child2-3", 2),
                new Node(2, "Parent2", -1),
        };

        System.out.println(buildGraphicalTree(nodes));
    }


    private static class Node{
        int id;
        String name;
        int parentId;

        public Node(int id, String name, int parentId) {
            this.id = id;
            this.name = name;
            this.parentId = parentId;
        }

    }

    public static String buildGraphicalTree(Node[] nodes){
        StringBuilder result = new StringBuilder();

        Arrays.sort(nodes, (a, b) -> a.name.compareTo(b.name));

        for (int i = 0; i < nodes.length; i++) {
            Node nodeParent = nodes[i];

            if(nodeParent.parentId == -1){
                result.append(nodeParent.name);
                result.append("\n");

                int generation = 1;

                search(nodes, nodeParent.id, generation, result);
            }
        }

        return result.toString();
    }

    public static void search(Node[] nodes, int parentId, int generation, StringBuilder result){
        for (int i = 0; i < nodes.length; i++) {
            Node node = nodes[i];
            if(node.parentId == parentId){

                for (int j = 0; j < generation; j++) {
                    result.append("- ");
                }

                result.append(node.name);
                result.append("\n");

                int id = node.id;
                int generationSun = generation + 1;

                search(nodes, id, generationSun, result);
            }
        }
    }


}
